const breweries = require('./src/breweries');
const stands = require('./src/stands');
const restaurants = require('./src/restaurants');

module.exports = {
  getBreweries: breweries.get,
  likeBrewery: breweries.like,
  getStands: stands.get,
  getRestaurants: restaurants.get,
};
