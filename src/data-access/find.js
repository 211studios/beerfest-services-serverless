/**
 * Created by aquaboy on 1/29/17.
 */

const mongodb = require('mongodb');

const { MongoClient } = mongodb.MongoClient;
const dburi = require('./config.json').beerfestMongoUri;

module.exports = (collectionName, criteria, size, offset, sortKey) => {
  size = size || 20;
  offset = offset || 0;
  criteria = criteria || {};
  return new Promise((resolve, reject) => {
    MongoClient.connect(dburi, (err, db) => {
      if (err) {
        reject(err);
      } else {
        const collection = db.collection(collectionName);
        collection.find(criteria).limit(size).skip(offset).sort(sortKey, 1)
          .toArray((ferr, docs) => {
            if (ferr) {
              reject(ferr);
            } else {
              resolve(docs);
            }
            db.close();
          });
      }
      db.close();
    });
  });
};
