/**
 * Created by aquaboy on 1/29/17.
 */
'use strict';

const event = require('./event.json');
const mocks = require('./mocks');
const mockery = require('mockery');
const chai = require('chai');

let getBreweries;

const expect = chai.expect;

describe('Get breweries', () => {
    before(() => {
        mockery.enable({
            warnOnReplace: true,
            warnOnUnregistered: true
        });
        mockery.registerAllowable('../../src/breweries/get.js', true);
    });

    after(() => {
        mockery.deregisterAll();
        mockery.disable();
    });

    it('Returns a list of breweries', (done) => {
        mockery.registerMock('../data-access', mocks.dataAccess);
        getBreweries = require('../../src/breweries/get.js');

        const callback = (err, payload) => {
            console.log(payload);
            mockery.deregisterMock('../data-access');
            expect(payload.statusCode).to.equal(200);
            done();
        };
        getBreweries({}, {}, callback);
    });

    it('Checks size and offset are numbers and works well if they are', (done) => {
        mockery.registerMock('../data-access', mocks.dataAccess);
        getBreweries = require('../../src/breweries/get.js');

        const callback = (err, payload) => {
            console.log(payload);
            mockery.deregisterMock('../data-access');
            expect(payload.statusCode).to.equal(200);
            done();
        };
        getBreweries(event, {}, callback);
    });

    it('Returns a 400 when size or offset are not numbers', (done) => {
        mockery.registerMock('../data-access', mocks.dataAccess);
        getBreweries = require('../../src/breweries/get.js');

        const callback = (err, payload) => {
            console.log(payload);
            mockery.deregisterMock('../data-access');
            expect(payload.statusCode).to.equal(400);
            done();
        };
        const weirdEvent = JSON.parse(JSON.stringify(event));
        weirdEvent.queryStringParameters.size = "Weirdness";
        getBreweries(weirdEvent, {}, callback);
    });

    it('Returns a 502 when the DB fails', (done) => {
        mockery.deregisterAllowable('../../src/breweries/get.js');
        mockery.registerAllowable('../../src/breweries/get.js', true);
        mockery.registerMock('../data-access', mocks.evilDataAccess);
        getBreweries = require('../../src/breweries/get.js');

        const callback = (err, payload) => {
            console.log(payload);
            mockery.deregisterMock('../data-access');
            expect(payload.statusCode).to.equal(502);
            done();
        };
        getBreweries(event, {}, callback);
    });
});
