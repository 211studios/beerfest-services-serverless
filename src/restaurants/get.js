/**
 * Created by alex on 10/23/17.
 */
const dataAccess = require('../data-access');

module.exports = (event, context, callback) => {
  dataAccess.find('Restaurant', event.queryParameters, null, null, 'name')
    .then(
      data => callback(null, { statusCode: 200, body: JSON.stringify(data) }),
      err => callback(null, { statusCode: 502, body: JSON.stringify(err) }),
    ).catch(err => callback(null, { statusCode: 500, body: JSON.stringify(err) }));
};
