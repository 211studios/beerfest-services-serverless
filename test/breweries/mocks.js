/**
 * Created by aquaboy on 1/29/17.
 */

const breweryData = require('./breweryData.json');

module.exports.dataAccess = {
    mockName : 'dataAccess',
    find: () => {
      return new Promise((resolve, reject) => {
          resolve(breweryData);
      })
    }
};

module.exports.evilDataAccess = {
    mockName : 'evilDataAccess',
    find: () => {
        return new Promise((resolve, reject) => {
            const err = new Error('Something weird happened', 'Database error')
            reject(err);
        })
    }
};

