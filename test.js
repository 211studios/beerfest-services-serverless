// By Alejandro Castro

const unitsDecimalsToDollar = (dlls, cents) => `You have $${dlls}.${cents.toString().substr(0, 2)}`;

const splitDollars = dollars => dollars.toString().split('.');

const dollarsToUnitsDecimals = dollars =>
  `You have ${splitDollars(dollars)[0]} dollars and ${splitDollars(dollars)[1].substr(0, 2)} cents`;

if (Number.isInteger(Number.parseInt(process.argv[2], 10))
  && Number.isInteger(Number.parseInt(process.argv[3], 10))) {
  console.log(unitsDecimalsToDollar(process.argv[2], process.argv[3]));
} else if (!Number.isNaN(Number.parseFloat(process.argv[2]))) {
  console.log(dollarsToUnitsDecimals(process.argv[2]));
} else {
  console.error('Invalid arguments');
}