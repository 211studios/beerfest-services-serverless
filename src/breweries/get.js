/**
 * Created by aquaboy on 1/28/17.
 */

const dataAccess = require('../data-access');

module.exports = (event, context, callback) => {
  console.log('Getting breweries');
  dataAccess.find('Brewery', event.queryParameters, null, null, 'name')
    .then(
      data => callback(null, { statusCode: 200, body: JSON.stringify(data) }),
      err => callback(null, { statusCode: 502, body: JSON.stringify(err) }),
    ).catch(err => callback(null, { statusCode: 500, body: JSON.stringify(err) }));
};
