/**
 * Created by aquaboy on 1/29/17.
 */

// Remember to send the field inside {} so that we can parse it
// Example  validateNumber({sizeField}), instead of
// validateNumber(sizeField)
const validateNumber = field => new Promise((resolve, reject) => {
  if (Number.isNaN(field[Object.keys(field)[0]])) {
    const err = new Error(`Field [${Object.keys(field)[0]}] must be a number`, 'Bad request');
    reject({ statusCode: 400, body: err.toString() });
  } else {
    resolve(Number.parseInt(field[Object.keys(field)[0]]));
  }
});

module.exports = {
  validateNumber,
};
